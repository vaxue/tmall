package com.wfx.tmall.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TmallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(TmallOrderApplication.class, args);
    }

}
