package com.wfx.tmall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TmallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(TmallCouponApplication.class, args);
    }

}
