package com.wfx.tmall.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TmallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(TmallWareApplication.class, args);
    }

}
