package com.wfx.tmall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TmallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(TmallMemberApplication.class, args);
    }

}
